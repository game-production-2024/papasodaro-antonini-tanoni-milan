using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SecondLevelManager : MonoBehaviour
{
    public bool isTransportingItem = false;
    public bool hasSoda = false;
    public bool hasGear = false;

    public GameObject puoiSoda;
    public GameObject puoiGear;
    public GameObject nonPuoiSoda;
    public GameObject nonPuoiGear;
    public GameObject dialogo1;
    public GameObject dialogo2;
    public GameObject dialogo3;

    private void Update()
    {
        if (hasSoda && hasGear)
        {
            dialogo2.SetActive(false);
            dialogo3.SetActive(true);
        }
    }

    public void SetTrasportingItem(bool value)
    {
        dialogo1.SetActive(false);
        dialogo2.SetActive(true);
        isTransportingItem = value;
        if (isTransportingItem)
        {
            if (hasSoda)
            {
                puoiGear?.SetActive(false);
                nonPuoiGear?.SetActive(true);
                //non puoi prendere la gear
                return;
            }
            if (hasGear)
            {
                puoiSoda?.SetActive(false);
                nonPuoiSoda?.SetActive(true);
                //non puoi prendere la soda
                return;
            }
        }
        else
        {
            if (hasSoda)
            {
                puoiGear?.SetActive(true);
                nonPuoiGear?.SetActive(false);
                //puoi portare la gear
                return;
            }
            if (hasGear)
            {
                puoiSoda?.SetActive(true);
                nonPuoiSoda?.SetActive(false);
                //puoi portare la soda
                return;
            }
        }
    }

    public void FoundSoda()
    {
        hasSoda = true;
    }

    public void FoundGear()
    {
        hasGear = true;
    }
}
