using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Instance;

    public bool SpawnerScript = false;




    private void   Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    static string fileName = "savefile.json";
    static string directory = @"C:\coordinate";
    static string path = Path.Combine(directory, fileName);

    public static string Coordinata { get; set; }

    public static Vector2 PlayerSpawnCoords;

    void Start()
    {
        Coordinata = ReadJson();
    }

    public void LoadGame()
    {

        //UnityEngine.JsonUtility.ToJson(Coordinata);
        //UnityEngine.JsonUtility.FromJson();

        
        var spawnCoords = Coordinata.Replace("(", "").Replace(")", "").Split(',');
        if (double.TryParse(spawnCoords[0], NumberStyles.Float, CultureInfo.InvariantCulture, out double xCoord)
           && double.TryParse(spawnCoords[1], NumberStyles.Float, CultureInfo.InvariantCulture, out double yCoord))
        {
            PlayerSpawnCoords = new Vector2((float)xCoord, (float)yCoord);
        }
        SceneManager.LoadScene(3);
    }


    public static void UpdateJson()
    {
        // Converto il valore in Json
        var content = JsonConvert.SerializeObject(Coordinata);

        // Scrivo il file dei risultati
        File.WriteAllText(path, content);
    }

    public static void DeleteJson()
    {
        Debug.Log ("sto cancellando la coordinata");
        // Scrivo il file dei risultati
        File.WriteAllText(path, "(0, 0)");

    }

    

    public  void SpawnerTrue ()
    {
        SpawnerScript = true;

    }
    public void SpawnerFalse()
    {
        SpawnerScript = false;

    }


    public static void UpdateCoordinata(UnityEngine.Vector2 coordinata)
    {
        var nuovaCordinata = coordinata.ToString();
        Coordinata = nuovaCordinata;
        UpdateJson();
    }

    private string ReadJson()
    {
        // Controllo che la cartella dei SaveFile esista
        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);
        }

        if (!File.Exists(path))
        {
            // Creo il file dei risultati
            using (File.Create(path)) { }
        }

        var fileContent = File.ReadAllText(path);
        var Nuovacoordinata = JsonConvert.DeserializeObject<string>(fileContent);
        var result = Nuovacoordinata == null ? "" : Nuovacoordinata;

        return result;
    }
}
