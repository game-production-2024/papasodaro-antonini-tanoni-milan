using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.Rendering.DebugUI;

public class MusicVolumeController : MonoBehaviour
{
    
    public Slider volumeSlider; // Riferimento allo slider

 
    void Start()
    {
       
            // Assicurati che lo slider parta con il valore corretto
            volumeSlider.value = SoundSystem.Instance.generalVolume;

            // Aggiungi un listener allo slider per chiamare la funzione OnVolumeChanged quando il valore cambia
            volumeSlider.onValueChanged.AddListener(OnVolumeChanged);
            Debug.Log("volume porca vacca");
    
    }
    
    public void OnVolumeChanged(float value)
    {
        Debug.Log("volume cambiatp :  " + value);
        SoundSystem.Instance.UpdateVolume(value);
    }
    
    }