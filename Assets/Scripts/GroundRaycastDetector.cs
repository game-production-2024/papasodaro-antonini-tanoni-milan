using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundRaycastDetector : MonoBehaviour
{
    public Transform rayDx;
    public Transform raySx;
    public LayerMask hookInteractionLayer;
    public PlayerParameters playerParameters;
    public float hight;

    void Update()
    {
        RaycastHit2D hitDx = Physics2D.Raycast(rayDx.position, Vector2.down, hight, hookInteractionLayer);
        RaycastHit2D hitSx = Physics2D.Raycast(raySx.position, Vector2.down, hight, hookInteractionLayer);

        Debug.DrawRay(rayDx.position, Vector2.down * hight, Color.blue);
        Debug.DrawRay(raySx.position, Vector2.down * hight, Color.blue);


        if ((hitDx.collider != null)||(hitSx.collider != null))
        {        }
        else
        {
        }
    }
}
