using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneOnTimer : MonoBehaviour
{
    public SceneLoader SceneLoader;
    public float ChangeTime;
    public int scena;
    public void Update()
    {
        ChangeTime -= Time.deltaTime;
        if (ChangeTime <= 0)
        {
            SceneLoader.LoadScene(scena);

        }




    }
}