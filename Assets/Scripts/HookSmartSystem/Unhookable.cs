using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unhookable : Hittable
{
    public override void InitializeHittable(Transform pivot, GameObject activeAnchor)
    {

    }

    public override void UpdateActiveAnchor(ref GameObject activeAnchor, ref Transform pivot, LineRenderer line, ref float hookedRopeLength)
    {

    }

    public override void DestroyHittable()
    {

    }
}
