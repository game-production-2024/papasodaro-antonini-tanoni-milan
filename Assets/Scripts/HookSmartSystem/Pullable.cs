using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Pullable : Hittable
{
    public Rigidbody2D rb;
    public DistanceJoint2D joint;

    public override void InitializeHittable(Transform pivot, GameObject activeAnchor)
    {
        activeAnchor.transform.SetParent(gameObject.transform);
        joint.enableCollision = true;
        joint.connectedAnchor = pivot.position;
        joint.enabled = true;
    }

    public override void UpdateActiveAnchor(ref GameObject activeAnchor, ref Transform pivot, LineRenderer line, ref float hookedRopeLength)
    {
        line.SetPosition(0, pivot.position);
        line.SetPosition(1, activeAnchor.transform.position);

        if ((pivot.position - transform.position).magnitude > 1)
        {
            if (Input.GetMouseButton(1))
            {
                joint.enabled = false;
                Vector2 forceDirection = (pivot.position - transform.position).normalized;
                rb.AddForceAtPosition(forceDirection * 2, activeAnchor.transform.position);
                hookedRopeLength = (pivot.position - transform.position).magnitude;
                joint.distance = (pivot.position - transform.position).magnitude;
            }
            else
            {
                joint.enabled = true;
            }
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }

    public override void DestroyHittable()
    {
        joint.enabled = false;
    }
}
