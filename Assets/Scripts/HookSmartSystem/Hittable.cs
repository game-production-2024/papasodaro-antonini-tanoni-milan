using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Hittable : MonoBehaviour
{
    public abstract void InitializeHittable(Transform pivot, GameObject activeAnchor);

    public abstract void UpdateActiveAnchor(ref GameObject activeAnchor, ref Transform pivot, LineRenderer line, ref float hookedRopeLength);

    public abstract void DestroyHittable();
}
