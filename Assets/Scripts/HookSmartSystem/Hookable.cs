using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hookable : Hittable
{
    [HideInInspector] public List<GameObject> anchorList = new List<GameObject>();
    [HideInInspector] public List<Transform> platformList = new List<Transform>();
    [HideInInspector] public List<float> lengths = new List<float>();
    [HideInInspector] public Vector2 startPosition;
    [HideInInspector] public Vector2 endPosition;
    [HideInInspector] public Vector2 direction;
    [HideInInspector] public float distance;
    [HideInInspector] public int listsIndex = -1;
    public LayerMask hookInteractionLayer;
    public GameObject anchorPrefab;

    public override void InitializeHittable(Transform pivot, GameObject activeAnchor)
    {

    }

    public override void UpdateActiveAnchor(ref GameObject activeAnchor, ref Transform pivot, LineRenderer line, ref float hookedRopeLength)
    {
        //Controlla se � necessaria una nuova ancora e nel caso creala
        startPosition = pivot.position;
        endPosition = activeAnchor.transform.position;

        direction = endPosition - startPosition;
        distance = direction.magnitude;
        direction = direction.normalized;

        RaycastHit2D hit = Physics2D.Raycast(startPosition, direction, distance - distance * 0.01f, hookInteractionLayer);

        Debug.DrawRay(startPosition, direction * (distance - distance * 0.01f));

        if (hit.collider != null)
        {
            anchorList.Add(activeAnchor);
            platformList.Add(hit.transform);
            lengths.Add(hookedRopeLength);
            listsIndex++;
            activeAnchor = Instantiate(anchorPrefab, hit.point, Quaternion.identity);
            hookedRopeLength = hookedRopeLength - (distance - hit.distance);
            //AddPoint(activeAnchor.transform.position, listsIndex + 1, line);
        }

        //Controlla se � necessario eliminare l'ultima ancora attiva e nel caso eliminala
        if (anchorList.Count > 0)
        {
            float det = Determinante(activeAnchor.transform.position, anchorList[listsIndex].transform.position, pivot.position);

            if (IsDestroyable(activeAnchor.transform.position, anchorList[listsIndex].transform.position, det))
            {
                Destroy(activeAnchor);
                activeAnchor = anchorList[listsIndex];
                hookedRopeLength = lengths[listsIndex];
                anchorList.RemoveAt(listsIndex);
                lengths.RemoveAt(listsIndex);
                listsIndex--;
                //RemovePoint(listsIndex + 1, line);
            }
        }

        DrawLine(line, pivot, activeAnchor);
    }

    public override void DestroyHittable()
    {
        foreach (GameObject anchor in anchorList)
        {
            Destroy(anchor);
        }

        anchorList.Clear();
        platformList.Clear();
        lengths.Clear();

        listsIndex = -1;
    }

    private float Determinante(Vector2 a, Vector2 b, Vector2 p)
    {
        return a.x * (b.y - p.y) + b.x * (p.y - a.y) + p.x * (a.y - b.y);
    }

    private bool IsDestroyable(Vector2 a, Vector2 b, float det)
    {
        if (a.y > platformList[listsIndex].transform.position.y)
        {
            if (a.x > platformList[listsIndex].transform.position.x)
            {
                if (((b.y >= a.y) && (det < 0)) || ((b.y < a.y) && (det > 0)))
                {
                    return true;
                }
            }
            else
            {
                if (((b.y >= a.y) && (det > 0)) || ((b.y < a.y) && (det < 0)))
                {
                    return true;
                }
            }
        }
        else
        {
            if (a.x > platformList[listsIndex].transform.position.x)
            {
                if (((b.y > a.y) && (det < 0)) || ((b.y <= a.y) && (det > 0)))
                {
                    return true;
                }
            }
            else
            {
                if (((b.y > a.y) && (det > 0)) || ((b.y <= a.y) && (det < 0)))
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void AddPoint(Vector3 newPoint, int index, LineRenderer line)
    {
        int pointCount = line.positionCount;

        if (index < 0 || index > pointCount)
        {
            Debug.LogError("Indice fuori dai limiti.");
            return;
        }

        line.positionCount = pointCount + 1;

        for (int i = pointCount; i > index; i--)
        {
            line.SetPosition(i, line.GetPosition(i - 1));
        }

        line.SetPosition(index, newPoint);
    }

    private void RemovePoint(int index, LineRenderer line)
    {
        int pointCount = line.positionCount;

        if (index < 0 || index >= pointCount)
        {
            Debug.LogError("Indice fuori dai limiti.");
            return;
        }

        for (int i = index; i < pointCount - 1; i++)
        {
            line.SetPosition(i, line.GetPosition(i + 1));
        }

        line.positionCount = pointCount - 1;
    }

    private void DrawLine(LineRenderer line, Transform pivot, GameObject activeAnchor)
    {
        int lenght = anchorList.Count + 2;
        line.positionCount = lenght;
        
        line.SetPosition(1, activeAnchor.transform.position);
        int n = 1;

        foreach (GameObject anchor in anchorList)
        {
            line.SetPosition(lenght - n, anchor.transform.position);
            n++;
        }
    }
}