using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Interactable : Hittable
{
    public Rigidbody2D rb;
    public DistanceJoint2D joint;

    public override void InitializeHittable(Transform pivot, GameObject activeAnchor)
    {
        
    }

    public override void UpdateActiveAnchor(ref GameObject activeAnchor, ref Transform pivot, LineRenderer line, ref float hookedRopeLength)
    {
        
    }

    public override void DestroyHittable()
    {

    }
}
