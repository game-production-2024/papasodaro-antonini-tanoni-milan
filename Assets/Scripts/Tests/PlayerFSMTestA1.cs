/*using UnityEngine;

public class PlayerFSMTestA1 : MonoBehaviour
{
    public IState currentState;

    void Start()
    {
        //Inizializzazione in stato Idle
        ChangeState(new IdleStateA1(this));
    }

    void Update()
    {
        currentState?.Execute();
    }

    void FixedUpdate()
    {
        
    }

    // Classe per il passaggio da uno stato ad un altro
    public void ChangeState(IState newState)
    {
        currentState?.Exit();
        currentState = newState;
        currentState.Enter();
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;

public class IdleStateA1 : IState
{
    private PlayerFSM splayerFSM;

    public IdleStateA1(PlayerFSM playerFSM)
    {
        splayerFSM = playerFSM;
    }

    public void Enter()
    {
        Debug.Log("Entering idle state");
    }

    public void Execute()
    {
        if (condizione)
        {
            splayerFSM.ChangeState(new NuovoStato(splayerFSM));
        }
    }

    public void FixedExecute()
    {

    }

    public void Exit()
    {
        Debug.Log("Exiting idle state");
    }
}
*/