using UnityEngine;

public class MovementController : MonoBehaviour
{
    public enum State : int
    {
        Idle, Walking, Jumping, Dashing, Attacking, Hooking
    }

    public State state;
    public float moveSpeed;
    public float walkSpeed = 5f;   // Velocit� di movimento
    public float fallSpeed = 2f;     // Velocit� di movimento in caduta libera
    public float horizontalMovement;

    private bool isFly = false;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        horizontalMovement = Input.GetAxisRaw("Horizontal");

        switch (state)
        {
            case State.Idle:
                break;

            case State.Walking:
                break;

            case State.Jumping:
                break;

            case State.Dashing:
                break;

            case State.Attacking:
                break;

            case State.Hooking:
                break;
        }


    }

    void FixedUpdate()
    {

        if (true)
        {
            state = State.Walking;
        }

        //movimento
        //transform.Translate(Vector3.right * horizontalMovement * moveSpeed * Time.deltaTime);
        rb.velocity = new Vector2(moveSpeed * horizontalMovement, rb.velocity.y);

        //flip del personaggio in base alla direzione di movimento
        if (horizontalMovement != 0)
        {
            transform.localScale = new Vector3(horizontalMovement, 1f, 1f);
        }

        //rallentamento del movimento dovuto alla caduta
        if (isFly&&(rb.velocity.y < 0))
        {
            moveSpeed = fallSpeed;
        }
        else 
        {
            moveSpeed = walkSpeed;
        }

        
    }

    public void SetIsFly (bool value)
    {
        isFly = value;
    }
}