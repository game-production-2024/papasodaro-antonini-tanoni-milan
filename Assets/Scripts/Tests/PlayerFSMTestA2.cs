/*using UnityEngine;

public class PlayerFSMTestA2 : MonoBehaviour
{
    public IState currentState;
    public GameObject idleStateObj;
    public IdleStateA2 idleState;

    void Start()
    {
        //Inizializzazione stato Idle
        idleState = idleStateObj.GetComponent<IdleStateA2>();
        if (idleState != null)
        {
            idleState(this);
        }
        //Inizializzazioni di tutti gli altri stati
        //
        //
        //
        //
        //
        //
        //Inizializzazione in stato Idle
        ChangeState(idleState);
    }

    void Update()
    {
        currentState?.Execute();
    }

    void FixedUpdate()
    {
        
    }

    // Classe per il passaggio da uno stato ad un altro
    public void ChangeState(IState newState)
    {
        currentState?.Exit();
        currentState = newState;
        currentState.Enter();
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;

public class IdleStateA2 : MonoBehaviour, IState
{
    private PlayerFSM splayerFSM;

    public IdleStateA1(PlayerFSM playerFSM)
    {
        splayerFSM = playerFSM;
    }

    public void Enter()
    {
        Debug.Log("Entering idle state");
    }

    public void Execute()
    {
        if (condizione)
        {
            splayerFSM.ChangeState(splayerFSM.NuovoStato);
        }
    }

    public void FixedExecute()
    {

    }

    public void Exit()
    {
        Debug.Log("Exiting idle state");
    }
}
*/