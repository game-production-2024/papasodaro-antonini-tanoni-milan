using UnityEngine;

public class FollowObjectWithRotation : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    public bool syncRotation = true;  // Opzione per sincronizzare la rotazione

    void Start()
    {
        if (target != null)
        {
            offset = transform.position - target.position;
        }
    }

    void Update()
    {
        if (target != null)
        {
            // Applica l'offset relativo alla rotazione del target
            transform.position = target.position + target.rotation * offset;

            // Sincronizza la rotazione se abilitato
            if (syncRotation)
            {
                transform.rotation = target.rotation;
            }
        }
    }
}
