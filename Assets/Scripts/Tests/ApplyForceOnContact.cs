using UnityEngine;

public class ApplyForceOnContact : MonoBehaviour
{
    public Rigidbody2D targetRigidbody;
    public Collider2D targetCollider;
    public Vector2 force = new Vector2(10f, 0f);  // La forza da applicare
    private bool isInContact = false;

    // Metodo chiamato quando un altro collider entra in contatto con il collider di questo oggetto
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider == targetCollider)
        {
            isInContact = true;
        }
    }

    // Metodo chiamato quando un altro collider smette di essere in contatto con il collider di questo oggetto
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider == targetCollider)
        {
            isInContact = false;
        }
    }

    // Metodo chiamato ad ogni frame
    private void FixedUpdate()
    {
        if (isInContact)
        {
            targetRigidbody.AddForce(force);
        }
    }
}
