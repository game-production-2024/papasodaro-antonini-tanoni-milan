using UnityEngine;

public class FSMoneScript : MonoBehaviour
{
    public enum State : int
    {
        Idle, Walking, Jumping, Dashing, Attacking, Hooking
    }

    public State state;

    void Start()
    {

    }

    private void Update()
    {
        switch (state)
        {
            case State.Idle:
                break;

            case State.Walking:
                break;

            case State.Jumping:
                break;

            case State.Dashing:
                break;

            case State.Attacking:
                break;

            case State.Hooking:
                break;
        }


    }

    void FixedUpdate()
    {
        if (true)
        {
            state = State.Walking;
        }
    }
}