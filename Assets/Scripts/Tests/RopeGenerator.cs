using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeGenerator : MonoBehaviour
{
    public GameObject ropeSegmentPrefab;
    public GameObject Pako;
    public Transform spawnPoint;
    public List<GameObject> rope;
    public int lastIndex;
    public Rigidbody2D rb;
    public Rigidbody2D prevrb;
    public Rigidbody2D firstrb;
    public HingeJoint2D hinge;
    public int k = 0;
    public Vector2 aimPosition;


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            spawnPoint.position = Pako.transform.position;

            // Ottieni la posizione del mouse
            aimPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            // Calcola la direzione dalla posizione dell'oggetto al mouse
            spawnPoint.transform.right = aimPosition - (Vector2)spawnPoint.position;

            k = 0;

            GenerateRope();
        }
    }

    public void GenerateRope()
    {
        StartCoroutine(Timer(3));
    }

    IEnumerator Timer(float seconds)
    {
        // Stampa un messaggio prima dell'attesa
        Debug.Log("Timer avviato...");

        // Attendi per il numero di secondi specificato
        yield return new WaitForSeconds(seconds);

        ExecuteAfterTimer();
    }

    void ExecuteAfterTimer()
    {
        rope.Add(Instantiate(ropeSegmentPrefab, spawnPoint.position, spawnPoint.rotation));
        
        lastIndex = rope.Count - 1;
        
        rb = rope[lastIndex].GetComponent<Rigidbody2D>();
        
        hinge = rope[lastIndex].GetComponent<HingeJoint2D>();
        
        if (lastIndex == 0)
        {
        
            Destroy(hinge);
            
            rb.gravityScale = 0;
            
            rb.velocity = aimPosition - (Vector2)transform.position;
            
            firstrb = rb;
        
        }
        else
        {
            
            rb.gravityScale = 0;
            
            prevrb = rope[lastIndex - 1].GetComponent<Rigidbody2D>();
            
            hinge.connectedBody = prevrb;
        
        }
        
        firstrb.velocity = aimPosition - (Vector2)transform.position;
        
        k++;
        
        if(k<20)
        {
            StartCoroutine(Timer(0.03f));
        }
    }
}
