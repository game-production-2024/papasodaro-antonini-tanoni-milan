using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Conveyor : MonoBehaviour
{
    [SerializeField] private string otherTag;
    public Rigidbody2D movable;
    public PlayerParameters playerParameters;
    public float moveSpeed;
    public bool isConveyorOn;
    public bool isPlayerOnConveyor;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (isConveyorOn && other.CompareTag(otherTag))
        {
            isPlayerOnConveyor = true;
            playerParameters.rightMoveSpeed = playerParameters.baseSpeed + moveSpeed;
            playerParameters.leftMoveSpeed = playerParameters.baseSpeed - moveSpeed;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (isConveyorOn && other.CompareTag(otherTag))
        {
            isPlayerOnConveyor = false;
            playerParameters.rightMoveSpeed = playerParameters.baseSpeed;
            playerParameters.leftMoveSpeed = playerParameters.baseSpeed;
        }
    }

    private void FixedUpdate()
    {
        if (isConveyorOn && isPlayerOnConveyor && (playerParameters.horizontalMovement == 0))
        {
            movable.velocity = new Vector2(moveSpeed, movable.velocity.y);
        }
    }

    public void TurnOnConveyor()
    {
        isConveyorOn = true;
    }
}
