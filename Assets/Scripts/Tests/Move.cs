using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public Rigidbody2D rb;
    public float moveSpeed;

    void FixedUpdate()
    {
        rb.velocity = new Vector2(moveSpeed, 0);
    }

    public void SetSpeed(int value)
    {
        moveSpeed = value;
    }
}
