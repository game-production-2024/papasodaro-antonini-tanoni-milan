using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private GameObject mainCharacter; //Personaggio da seguire
    [SerializeField] private float boxx; //Dimensione della "camera box" sulla coordinata x
    [SerializeField] private float boxy; //Dimensione della "camera box" sulla coordinata y

    void Update()
    {
        //Riposizionamento camera sulla x
        if (Mathf.Abs(mainCharacter.transform.position.x - transform.position.x) > boxx)
        {
            if (mainCharacter.transform.position.x > transform.position.x)
            {
                transform.position = new Vector3(mainCharacter.transform.position.x - boxx, transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(mainCharacter.transform.position.x + boxx, transform.position.y, transform.position.z);
            }
        }
        //Riposizionamento camera sulla y
        if (Mathf.Abs(mainCharacter.transform.position.y - transform.position.y) > boxy)
        {
            if (mainCharacter.transform.position.y > transform.position.y)
            {
                transform.position = new Vector3(transform.position.x, mainCharacter.transform.position.y - boxy, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, mainCharacter.transform.position.y + boxy, transform.position.z);
            }
        }
    }
}
