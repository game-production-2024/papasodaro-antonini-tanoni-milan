using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Rendering;

public class PlayerHookGround : BaseState
{
    public bool isChanged;

    protected override void OnEnter()
    {
        playerParameters.rb.velocity = Vector2.zero;

        grapplingHookController.InitializeHook();
    }

    protected override void OnUpdate()
    {
        playerParameters.horizontalMovement = Input.GetAxisRaw("Horizontal");
        grapplingHookController.UpdateHook();

        if (!Input.GetMouseButton(0))
        {
            grapplingHookController.DestroyHook();
            fsm.ChangeState<PlayerIdle>();
            return;
        }

        if ((!playerParameters.IsGrounded()) ||(Input.GetKeyDown(playerParameters.jumpKey)))
        {
            fsm.ChangeState<PlayerHookAir>();
            return;
        }

        if (Input.GetKey(KeyCode.W) || Input.GetMouseButton(1))
        {
            fsm.ChangeState<PlayerHookRollUp>();
        }
    }

    protected override void OnFixedUpdate()
    {
        switch (grapplingHookController.SignedDist()) 
        {
            case 0:
                if (playerParameters.horizontalMovement == 0)
                {
                    if (!isChanged)
                    {
                        playerParameters.rb.velocity = new Vector2(0, playerParameters.rb.velocity.y);
                        isChanged = true;
                    }
                    return;
                }
                isChanged = false;
                playerParameters.rb.velocity = new Vector2(playerParameters.MoveSpeed() * playerParameters.horizontalMovement, playerParameters.rb.velocity.y);
                break;
            case int dist when dist == playerParameters.horizontalMovement:
                playerParameters.rb.velocity = new Vector2(playerParameters.MoveSpeed() * playerParameters.horizontalMovement, playerParameters.rb.velocity.y);
                break;
            case int dist when dist == -playerParameters.horizontalMovement:
                playerParameters.rb.velocity = new Vector2(0, playerParameters.rb.velocity.y);
                break;
        }
    }

    protected override void OnExit()
    {

    }
}
