using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class PlayerIdle : BaseState
{
    protected override void OnEnter()
    {
        playerParameters.rb.velocity = Vector2.zero;
    }

    protected override void OnUpdate()
    {
        playerParameters.horizontalMovement = Input.GetAxisRaw("Horizontal");

        if (playerParameters.horizontalMovement != 0)
        {
            fsm.ChangeState<PlayerWalk>();
        }

        if ((!playerParameters.IsGrounded())||((Input.GetKeyDown(playerParameters.jumpKey)&&(playerParameters.IsGrounded()))))
        {
            fsm.ChangeState<PlayerMidAir>();
        }

        if (Input.GetMouseButtonDown(0))
        {
            grapplingHookController.TryShootingHook();
        }
    }

    protected override void OnFixedUpdate()
    {

    }
    
    protected override void OnExit()
    {

    }
}
