using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWalk : BaseState
{
    protected override void OnEnter()
    {

    }

    protected override void OnUpdate()
    {
        playerParameters.horizontalMovement = Input.GetAxisRaw("Horizontal");

        if ((!playerParameters.IsGrounded()) || (Input.GetKeyDown(playerParameters.jumpKey)))
        {
                fsm.ChangeState<PlayerMidAir>();
        }

        if (Input.GetMouseButtonDown(0))
        {
            grapplingHookController.TryShootingHook();
        }
    }

    protected override void OnFixedUpdate()
    {
        playerParameters.rb.velocity = new Vector2(playerParameters.MoveSpeed() * playerParameters.horizontalMovement, playerParameters.rb.velocity.y);

        if (playerParameters.horizontalMovement != 0)
        {
            playerParameters.GFXDirectionalFlips();
        }

        if (playerParameters.horizontalMovement == 0)
        {
            fsm.ChangeState<PlayerIdle>();
        }
    }

    protected override void OnExit()
    {

    }
}