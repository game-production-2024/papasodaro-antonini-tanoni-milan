using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : BaseState
{
    public DialogueManager dialogueManager;

    protected override void OnEnter()
    {
        playerParameters.rb.velocity = Vector2.zero;

        grapplingHookController.DestroyHook();
        grapplingHookController.DestroyJoint();

        animator.SetBool("IsMoving", false);
        animator.SetBool("IsSwinging", false);
        animator.SetBool("IsAirborne", false);
        animator.Play("Morte");

        SoundSystem.Instance.PlaySoundEffect("Morte");
    }

    protected override void OnUpdate()
    {
       
    }

    protected override void OnFixedUpdate()
    {

    }

    protected override void OnExit()
    {
       
    }
}
