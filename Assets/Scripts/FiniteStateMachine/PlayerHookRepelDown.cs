using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHookRepelDown : BaseState
{
    protected override void OnEnter()
    {
        grapplingHookController.SetJoint();
    }

    protected override void OnUpdate()
    {
        grapplingHookController.UpdateJoint();

        if (!Input.GetMouseButton(0))
        {
            grapplingHookController.DestroyHook();
            if (playerParameters.IsGrounded())
            {
                fsm.ChangeState<PlayerIdle>();
            }
            else
            {
                fsm.ChangeState<PlayerMidAir>();
            }
            return;
        }

        if (!Input.GetKey(KeyCode.S))
        {
            if (playerParameters.IsGrounded())
            {
                fsm.ChangeState<PlayerHookGround>();
            }
            else
            {
                fsm.ChangeState<PlayerHookAir>();
            }
        }
    }

    protected override void OnFixedUpdate()
    {
        grapplingHookController.RepelOnRope();
    }

    protected override void OnExit()
    {
        grapplingHookController.DestroyJoint();
    }
}
