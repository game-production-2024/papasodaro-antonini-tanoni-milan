using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHookRollUp : BaseState
{
    protected override void OnEnter()
    {
        grapplingHookController.SetJoint();
    }

    protected override void OnUpdate()
    {
        grapplingHookController.UpdateJoint();

        if (!Input.GetMouseButton(0))
        {
            grapplingHookController.DestroyHook();
            if (playerParameters.IsGrounded())
            {
                fsm.ChangeState<PlayerIdle>();
            }
            else
            {
                fsm.ChangeState<PlayerMidAir>();
            }
            return;
        }

        if (!Input.GetKey(KeyCode.W) && !Input.GetMouseButton(1))
        {
            if (playerParameters.IsGrounded())
            {
                fsm.ChangeState<PlayerHookGround>();
            }
            else
            {
                fsm.ChangeState<PlayerHookAir>();
            }
        }
    }

    protected override void OnFixedUpdate()
    {
        grapplingHookController.PullOnRope();
    }

    protected override void OnExit()
    {
        grapplingHookController.DestroyJoint();
    }
}
