using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class BaseState : MonoBehaviour
{
    protected FiniteStateMachine fsm;
    protected Animator animator;
    protected PlayerParameters playerParameters;
    protected GrapplingHookController grapplingHookController;
    public UnityEvent OnStateEnter;
    public UnityEvent OnStateExit;

    public void EnterState()
    {
        OnEnter();
        OnStateEnter?.Invoke();
    }

    public void UpdateState()
    {
        OnUpdate();
    }

    public void FixedUpdateState()
    {
        OnFixedUpdate();
    }
    public void ExitState()
    {
        OnExit();
        OnStateExit?.Invoke();
    }

    public void InitializeStates(FiniteStateMachine fsm, PlayerParameters playerParameters, GrapplingHookController grapplingHookController, Animator animator)
    {
        this.fsm = fsm;
        this.playerParameters = playerParameters;
        this.grapplingHookController = grapplingHookController;
        this.animator = animator;
    }

    protected abstract void OnEnter();
    protected abstract void OnUpdate();
    protected abstract void OnFixedUpdate();
    protected abstract void OnExit();

}
