using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiniteStateMachine : MonoBehaviour
{
    public BaseState currentState;
    public List<BaseState> states = new List<BaseState>();
    public Animator animator;
    public PlayerParameters playerParameters;
    public GrapplingHookController grapplingHookController;

    void Awake()
    {
        states.Clear();
         
        BaseState[] childrenStates = GetComponentsInChildren<BaseState>();

        foreach (BaseState state in childrenStates)
        {
            states.Add(state);
            state.InitializeStates(this, playerParameters, grapplingHookController, animator);
        }

        currentState = states[0];
    }

    public void ChangeState(BaseState newState)
    {
        if (states.Contains(newState))
        {
            Debug.Log($"Exiting {currentState.gameObject.name} state");
            currentState.ExitState();
            currentState = newState;
            currentState.EnterState();
            Debug.Log($"Entering {currentState.gameObject.name} state");
        }
        else
        {
            Debug.LogError($"Lo stato {newState.gameObject.name} non esiste");
        }
    }

    public void ChangeState<T>() where T : BaseState
    {
        foreach (BaseState state in states)
        {
            if (state is T)
            {
                ChangeState(state);
                return;
            }
        }
        Debug.LogError($"Il tipo dello stato scelto non � presente");
    }

    void Update()
    {
        currentState.UpdateState();
    }

    private void FixedUpdate()
    {
        currentState.FixedUpdateState();
    }
}