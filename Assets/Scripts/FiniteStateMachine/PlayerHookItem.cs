using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHookItem : BaseState
{
    protected override void OnEnter()
    {
        grapplingHookController.InitializeHook();

        playerParameters.animator.SetBool("IsPulling", true);        
    }
    protected override void OnUpdate()
    {
        playerParameters.horizontalMovement = Input.GetAxisRaw("Horizontal");
        grapplingHookController.UpdateHook();

        if (!Input.GetMouseButton(0))
        {
            grapplingHookController.DestroyHook();
            fsm.ChangeState<PlayerIdle>();
        }
    }
    
    protected override void OnFixedUpdate()
    {

    }

    protected override void OnExit()
    {
        playerParameters.animator.SetBool("IsPulling", false);
        grapplingHookController.DestroyHook();
    }
}