using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class PlayerMidAir : BaseState
{
    public bool jumpAllow;
    protected override void OnEnter()
    {
        jumpAllow = false;
        if (Input.GetKeyDown(playerParameters.jumpKey))
        {
            playerParameters.rb.AddForce(Vector2.up * playerParameters.jumpForce, ForceMode2D.Impulse);
            SoundSystem.Instance.PlaySoundEffect("Jump");
        }
        StartCoroutine(JumpAllowed(0.1f));
    }

    protected override void OnUpdate()
    {
        playerParameters.horizontalMovement = Input.GetAxisRaw("Horizontal");

        if (playerParameters.IsGrounded() && jumpAllow)
        {
            fsm.ChangeState<PlayerIdle>();
        }

        if (Input.GetMouseButtonDown(0))
        {
            grapplingHookController.TryShootingHook();
        }
    }

    protected override void OnFixedUpdate()
    {   
        playerParameters.rb.AddForce(new Vector2(0,-playerParameters.downwardForce),ForceMode2D.Force);

        if (playerParameters.horizontalMovement != 0)
        {
            playerParameters.pivot.localScale = new Vector3(playerParameters.horizontalMovement, 1f, 1f);

            if (playerParameters.rb.velocity.x <= playerParameters.MoveSpeed())
            {
                playerParameters.rb.velocity = new Vector2(playerParameters.MoveSpeed() * playerParameters.horizontalMovement, playerParameters.rb.velocity.y);
            }
        }
        else
        {
            playerParameters.rb.velocity = new Vector2(playerParameters.rb.velocity.x * playerParameters.decelerationFactor, playerParameters.rb.velocity.y);
        }
    }

    protected override void OnExit()
    {

    }

    IEnumerator JumpAllowed(float delay)
    {
        // Wait for the specified delay
        yield return new WaitForSeconds(delay);

        jumpAllow = true;
    }
}
