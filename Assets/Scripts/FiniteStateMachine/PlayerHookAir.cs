using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHookAir : BaseState
{
    protected override void OnEnter()
    {
        if (Input.GetKeyDown(playerParameters.jumpKey))
        {
            playerParameters.rb.AddForce(Vector2.up * playerParameters.jumpForce, ForceMode2D.Impulse);
            SoundSystem.Instance.PlaySoundEffect("Jump");
        }

        grapplingHookController.InitializeHook();
    }

    protected override void OnUpdate()
    {
        playerParameters.horizontalMovement = Input.GetAxisRaw("Horizontal");

        if (grapplingHookController.joint != null)
        {
            grapplingHookController.UpdateJoint();
            
            if (Input.GetKey(KeyCode.W) || Input.GetMouseButton(1))
            {
                fsm.ChangeState<PlayerHookRollUp>();
                return;
            }

            if (Input.GetKey(KeyCode.S))
            {
                fsm.ChangeState<PlayerHookRepelDown>();
                return;
            }
        }    
        else
        {
            grapplingHookController.UpdateHook();
        }

        if (!Input.GetMouseButton(0))
        {
            grapplingHookController.DestroyHook();
            fsm.ChangeState<PlayerMidAir>();
            return;
        }

        if (playerParameters.IsGrounded())
        {
            fsm.ChangeState<PlayerHookGround>();
            return;
        }
    }

    protected override void OnFixedUpdate()
    {

        //if (grapplingHookController.joint == null)
        //{
        //    playerParameters.rb.velocity = new Vector2(playerParameters.moveSpeed * playerParameters.horizontalMovement, playerParameters.rb.velocity.y);
        //    playerParameters.rb.AddForce(new Vector2(0,-playerParameters.downwardForce), ForceMode2D.Force);
        if ((grapplingHookController.SignedDist() != 0) && (grapplingHookController.joint == null))
        {
            grapplingHookController.SetJoint();
        }
        //}
        //else
        {
            Vector2 force = new Vector2(playerParameters.horizontalMovement * grapplingHookController.ApplySwingForce(), 0);
            playerParameters.rb.AddForce(force);
            if (grapplingHookController.SignedDist() == 0)
            {
                grapplingHookController.DestroyJoint();
            }
        }

    }

    protected override void OnExit()
    {
        grapplingHookController.DestroyJoint();
    }
}
