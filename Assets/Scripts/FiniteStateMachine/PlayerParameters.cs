using System.Collections;
using System.Collections.Generic;
using System.Runtime.Versioning;
using UnityEngine;
using UnityEngine.Rendering;

public enum Version
{
    Versione1, Versione2, Versione3, Versione4
}

public class PlayerParameters : MonoBehaviour
{
    //Variables
    [Header("Controls")]
    public KeyCode jumpKey = KeyCode.Space;

    [Header("Tunables")]
    public float baseSpeed;
    public float rightMoveSpeed;
    public float leftMoveSpeed;
    public float decelerationFactor;
    public float jumpForce;
    [Tooltip("The force applied when in air, higher force lower time of jump")] public float downwardForce;
    [Tooltip("The maximum force that can be applied to push Pako while in the air and grappled")] public float maxForce = 3f;
    public float pullSpeed;
    public float repelSpeed;
    public float maxRopeLenght;

    [Header("Resources")]
    public int goodnessOfPako = 0;
    public bool isBossFight = false;
    public Version version;
    public GameObject physicalPlayer;
    public LineRenderer line;
    public Rigidbody2D rb;
    public Transform pivot;
    public Animator animator;
    public GameObject arrow;
    public Transform rayDx;
    public Transform raySx;
    public CatMovement cat;
    public GameObject cameraBase;
    public GameObject cameraBoss;

    [Header("Arm Sprites")]
    public bool isArmBroken;
    public SpriteRenderer brokenArmDx;
    public SpriteRenderer brokenArmSx;
    public SpriteRenderer normalArmDx;
    public SpriteRenderer normalArmSx;
    public SpriteRenderer hookArmDx;
    public SpriteRenderer hookArmSx;
    public SpriteRenderer hookArmP1Dx;
    public SpriteRenderer hookArmP2Dx;
    public SpriteRenderer hookArmP3Dx;
    public SpriteRenderer hookArmP1Sx;
    public SpriteRenderer hookArmP2Sx;
    public SpriteRenderer hookArmP3Sx;
    public SpriteRenderer hookArmSwingDx;
    public SpriteRenderer hookArmSwingSx;

    [Header("Debug")]
    public float horizontalMovement;
    public Vector2 resetPosition;
    public FiniteStateMachine fsm;
    public GrapplingHookController grapplingHookController;
    public LayerMask hookInteractionLayer;

    [Header("Do Not Touch")]
    public float hight;


    // Functions
    public bool IsGrounded()
    {
        RaycastHit2D hitDx = Physics2D.Raycast(rayDx.position, Vector2.down, hight, hookInteractionLayer);
        RaycastHit2D hitSx = Physics2D.Raycast(raySx.position, Vector2.down, hight, hookInteractionLayer);

        Debug.DrawRay(rayDx.position, Vector2.down * hight, Color.blue);
        Debug.DrawRay(raySx.position, Vector2.down * hight, Color.blue);


        if ((hitDx.collider != null) || (hitSx.collider != null))
        {
            animator.SetBool("IsAirborne", false);
            return true;
        }
        else
        {
            animator.SetBool("IsAirborne", true);
            return false;
        }
    }

    public float MoveSpeed()
    {
        if (horizontalMovement == 1)
            return rightMoveSpeed;
        if (horizontalMovement == -1)
            return leftMoveSpeed;
        return 0;
    }

    public void GFXDirectionalFlips()
    {
        pivot.localScale = new Vector3(horizontalMovement, 1f, 1f);
        if (isArmBroken)
        {
            if (horizontalMovement == 1)
            {
                brokenArmDx.enabled = true;
                brokenArmSx.enabled = false;
                normalArmDx.enabled = false;
                normalArmSx.enabled = true;
            }
            else if (horizontalMovement == -1)
            {
                brokenArmDx.enabled = false;
                brokenArmSx.enabled = true;
                normalArmDx.enabled = true;
                normalArmSx.enabled = false;
            }
        }
        else
        {
            if (horizontalMovement == 1)
            {
                FacingRight();
            }
            else if (horizontalMovement == -1)
            {
                FacingLeft();
            }
        }
    }

    public void FacingRight()
    {
        SetRightHook(true);
        SetLeftHook(false);
        normalArmDx.enabled = false;
        normalArmSx.enabled = true;
        hookArmSwingDx.enabled = true;
        hookArmSwingSx.enabled = false;
    }

    public void FacingLeft() 
    {
        SetRightHook(false);
        SetLeftHook(true);
        normalArmDx.enabled = true;
        normalArmSx.enabled = false;
        hookArmSwingDx.enabled = false;
        hookArmSwingSx.enabled = true;
    }    

    public void SetRightHook(bool value)
    {
            hookArmDx.enabled = value;
            hookArmP1Dx.enabled = value;
            hookArmP2Dx.enabled = value;
            hookArmP3Dx.enabled = value;
    }

    public void SetLeftHook(bool value) 
    {
        hookArmSx.enabled = value;
        hookArmP1Sx.enabled = value;
        hookArmP2Sx.enabled = value;
        hookArmP3Sx.enabled = value;
    }

    public void AddPakoGoodness(int value)
    {
        goodnessOfPako += value;
    }

    public void StartBossFight()
    {
        isBossFight = true;
    }

    public void SetCheckpoint(Transform value)
    {
        resetPosition = value.position;
        SaveManager.UpdateCoordinata(value.position);
    }

    public void DeathReset()
    {
        fsm.ChangeState<PlayerDeath>();
    }

    public void Update()
    {
        if (rb.velocity == Vector2.zero)
        {
            animator.SetBool("IsMoving", false);
        }
        else
        {
            animator.SetBool("IsMoving", true);
        }

        if(isBossFight)
        {
            cameraBase.SetActive(false);
            cameraBoss.SetActive(true);
        }
        else
        {
            cameraBase.SetActive(true);
            cameraBoss.SetActive(false);
        }
    }
}
