using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTalking : BaseState
{
    public DialogueManager dialogueManager;

    protected override void OnEnter()
    {
        animator.Play("Talking");
        playerParameters.rb.velocity = Vector2.zero;
        playerParameters.arrow.SetActive(false);
    }

    protected override void OnUpdate()
    {
        if (!dialogueManager.isDialogueActive) 
        {
            fsm.ChangeState<PlayerIdle>();
        }
    }

    protected override void OnFixedUpdate()
    {

    }

    protected override void OnExit()
    {
        animator.Play("Idle_Rampino_Corretta");
        playerParameters.arrow.SetActive(true);
    }
}
