﻿using System.Collections;
using UnityEngine;

public class DeactivateAfterDelay : MonoBehaviour
{
    public GameObject objectToDeactivate;  // Reference to the GameObject to deactivate
    void OnEnable()
    {
        if (objectToDeactivate != null)
        {
            // Start the coroutine to deactivate the object after 0.2 seconds
            StartCoroutine(DeactivateAfterTime(0.2f));
        }
        else
        {
            Debug.LogError("objectToDeactivate is not set.");
        }
    }

    IEnumerator DeactivateAfterTime(float delay)
    {
        // Wait for the specified delay
        yield return new WaitForSeconds(delay);

        // Deactivate the GameObject
        objectToDeactivate.SetActive(false);
        Debug.Log(objectToDeactivate.name + " has been deactivated.");
    }
}
