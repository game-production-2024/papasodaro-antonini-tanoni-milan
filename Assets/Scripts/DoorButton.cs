using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class DoorButton : MonoBehaviour
{
    public GameObject PortaDaAprire;
    public SpriteRenderer spriterenderer;
    public Sprite Spento;
    public Sprite Acceso;
    public GameObject Oggetto;
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            PortaDaAprire.SetActive(false);
            spriterenderer.sprite = Acceso;
        }
       
    }
     
    public void SetActive ()
    {
        Oggetto.SetActive(true);
    }
    public void Deactivate()
    {
        Oggetto.SetActive(false);
    }
}
