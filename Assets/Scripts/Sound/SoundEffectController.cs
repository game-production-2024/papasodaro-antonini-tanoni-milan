using UnityEngine;

public class SoundEffectController : MonoBehaviour
{
    // Nome dell'effetto sonoro che vuoi riprodurre (es. "Footsteps")
    public string soundEffectName;

    // Funzione per riprodurre l'effetto sonoro specificato
    public void PlaySoundEffect()
    {
        if (!string.IsNullOrEmpty(soundEffectName))
        {
            SoundSystem.Instance.PlaySoundEffect(soundEffectName);
        }
        else
        {
            Debug.LogWarning("No sound effect name provided.");
        }
    }

    // Funzione per riprodurre un effetto sonoro specifico passando un nome
    public void PlaySpecificSoundEffect(string effectName)
    {
        if (!string.IsNullOrEmpty(effectName))
        {
            SoundSystem.Instance.PlaySoundEffect(effectName);
        }
        else
        {
            Debug.LogWarning("No sound effect name provided.");
        }
    }
}
