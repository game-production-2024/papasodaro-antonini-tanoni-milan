using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundSystem : MonoBehaviour
{
    // Singleton instance
    public static SoundSystem Instance;

    // Soundtracks
    public List<AudioClip> soundtracks;
    public AudioSource soundtrackSource;  // Collegherai questo AudioSource dall'Inspector

    // Queue for soundtracks
    private Queue<AudioClip> soundtrackQueue;

    // General volume (influenzer� solo gli effetti sonori)
    [Range(0f, 1f)]
    public float generalVolume = 1.0f;

    // Volume per le soundtrack
    [Range(0f, 1f)]
    public float soundtrackVolume = 0.33f;

    // Fade duration
    public float fadeDuration = 1.0f;

    // Sound Effects
    [System.Serializable]
    public class SoundEffect
    {
        public string name;
        public List<AudioClip> clips;  // Randomized sound clips (e.g., for footsteps)
        public AudioSource source;
        public bool followGeneralVolume = true;
    }

    public List<SoundEffect> soundEffects;

    // Public property to access the soundtrackSource
    public AudioSource SoundtrackSource
    {
        get { return soundtrackSource; }
    }

    private void Awake()
    {
        // Singleton setup
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        soundtrackQueue = new Queue<AudioClip>();

        // Set initial volume
        UpdateVolume(1f);
    }

    // Update the volume for sound effects and general volume, but not for the soundtrack
    public void UpdateVolume(float value)
    {
        generalVolume = value;

        foreach (var effect in soundEffects)
        {
            if (effect.followGeneralVolume)
            {
                effect.source.volume = generalVolume;
            }
        }
    }

    // Play soundtrack by name with fade in/out
    public void PlaySoundtrack(string soundtrackName)
    {
        AudioClip selectedClip = soundtracks.Find(track => track.name == soundtrackName);
        if (selectedClip != null)
        {
            StartCoroutine(FadeOutAndPlayNewSoundtrack(selectedClip));
        }
        else
        {
            Debug.LogWarning("Soundtrack not found: " + soundtrackName);
        }
    }

    // Play soundtrack instantly, bypassing queue, with fade in/out
    public void PlaySoundtrackInstantly(string soundtrackName)
    {
        AudioClip selectedClip = soundtracks.Find(track => track.name == soundtrackName);
        if (selectedClip != null)
        {
            soundtrackQueue.Clear();  // Clear the queue
            StartCoroutine(FadeOutAndPlayNewSoundtrack(selectedClip));
        }
        else
        {
            Debug.LogWarning("Soundtrack not found: " + soundtrackName);
        }
    }

    // Clear the soundtrack queue
    public void ClearSoundtrackQueue()
    {
        soundtrackQueue.Clear();
    }

    // Play random sound effect by name (e.g., footsteps)
    public void PlaySoundEffect(string soundEffectName)
    {
        SoundEffect soundEffect = soundEffects.Find(effect => effect.name == soundEffectName);
        if (soundEffect != null && soundEffect.clips.Count > 0)
        {
            int randomIndex = Random.Range(0, soundEffect.clips.Count);
            soundEffect.source.clip = soundEffect.clips[randomIndex];
            soundEffect.source.Play();
        }
        else
        {
            Debug.LogWarning("Sound effect not found or no clips assigned: " + soundEffectName);
        }
    }

    // Fade out the current soundtrack and play a new one
    private IEnumerator FadeOutAndPlayNewSoundtrack(AudioClip newSoundtrack)
    {
        // Fade out
        if (soundtrackSource.isPlaying)
        {
            float startVolume = soundtrackSource.volume;
            for (float t = 0; t < fadeDuration; t += Time.deltaTime)
            {
                soundtrackSource.volume = Mathf.Lerp(startVolume, 0, t / fadeDuration);
                yield return null;
            }
            soundtrackSource.Stop();
        }

        // Play new soundtrack
        soundtrackSource.clip = newSoundtrack;
        soundtrackSource.Play();

        // Fade in
        for (float t = 0; t < fadeDuration; t += Time.deltaTime)
        {
            soundtrackSource.volume = Mathf.Lerp(0, soundtrackVolume, t / fadeDuration);
            yield return null;
        }

        soundtrackSource.volume = soundtrackVolume;
    }

    // Update soundtrack if current one finishes
    private void Update()
    {
        if (!soundtrackSource.isPlaying)
        {
            if (soundtrackQueue.Count > 0)
            {
                // Se ci sono altre canzoni in coda, continua a riprodurre la prossima
                AudioClip nextSoundtrack = soundtrackQueue.Dequeue();
                StartCoroutine(FadeOutAndPlayNewSoundtrack(nextSoundtrack));
            }
            else
            {
                // Se la coda � vuota, riproduci di nuovo la stessa traccia
                if (soundtrackSource.clip != null)
                {
                    StartCoroutine(FadeOutAndPlayNewSoundtrack(soundtrackSource.clip));
                }
            }
        }
    }
}
