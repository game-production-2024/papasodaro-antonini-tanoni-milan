using UnityEngine;

public class SoundtrackController : MonoBehaviour
{
    // Enum for the different soundtrack actions
    public enum SoundtrackAction
    {
        Play,
        PlayInstantly,
        Stop,
        ClearQueue
    }

    // Name of the soundtrack to play (only used for Play and PlayInstantly actions)
    public string soundtrackName;

    // Trigger the action on start
    public bool triggerOnStart = false;

    private void Start()
    {
        if (triggerOnStart)
        {
            PlaySoundtrack();  // Default action
        }
    }

    // Public method to play soundtrack
    public void PlaySoundtrack()
    {
        if (!string.IsNullOrEmpty(soundtrackName))
        {
            SoundSystem.Instance.PlaySoundtrack(soundtrackName);
        }
        else
        {
            Debug.LogWarning("No soundtrack name provided for Play action.");
        }
    }

    // Public method to play soundtrack instantly (bypass queue)
    public void PlaySoundtrackInstantly()
    {
        if (!string.IsNullOrEmpty(soundtrackName))
        {
            SoundSystem.Instance.PlaySoundtrackInstantly(soundtrackName);
        }
        else
        {
            Debug.LogWarning("No soundtrack name provided for PlayInstantly action.");
        }
    }

    // Public method to stop the current soundtrack
    public void StopSoundtrack()
    {
        // Use the public property to access the soundtrackSource
        SoundSystem.Instance.SoundtrackSource.Stop();
    }

    // Public method to clear the soundtrack queue
    public void ClearSoundtrackQueue()
    {
        SoundSystem.Instance.ClearSoundtrackQueue();
    }
}
