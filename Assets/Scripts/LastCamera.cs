using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class LastCamera : MonoBehaviour
{
    public float time;
    public float finalSize;
    private float elapsedTime = 0;
    private float t;
    public Transform cameraFollowPosition;
    public Transform startPosition;
    public Transform endPostion;
    public CinemachineVirtualCamera virtualCamera;

    public void LastCamZoom()
    {
        StartCoroutine(DoLastCamZoom());
    }

    public void LastCamMove()
    {
        StartCoroutine(DoLastCamMove());
    }

    public IEnumerator DoLastCamZoom()
    {
        Time.timeScale = 0f; 

        while (elapsedTime < time)
        {
            elapsedTime += Time.unscaledDeltaTime;
            t = Mathf.Clamp01(elapsedTime / time);
            virtualCamera.m_Lens.OrthographicSize = Mathf.Lerp(7f, finalSize, t);
            yield return null;  // Attende fino al frame successivo
        }

        Time.timeScale = 1f; 
    }

    public IEnumerator DoLastCamMove()
    {
        Time.timeScale = 0f;

        while (elapsedTime < time)
        {
            startPosition = cameraFollowPosition;
            elapsedTime += Time.unscaledDeltaTime;
            t = Mathf.Clamp01(elapsedTime / time);
            cameraFollowPosition.position = Vector2.Lerp(startPosition.position, endPostion.position, t);
            yield return null;  // Attende fino al frame successivo
        }

        Time.timeScale = 1f;
    }
}
