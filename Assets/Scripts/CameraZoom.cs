using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    public float finalSize;
    private float elapsedTime = 0;
    private float halfTime;
    private float t = 0;
    public float duration;
    public CinemachineVirtualCamera virtualCamera;

    public void DoZoom()
    {
        StartCoroutine(Zoom());
    }

    private IEnumerator Zoom()
    {
        // Calcola il tempo a met� della durata
        halfTime = duration / 2;

        // Prima fase: pan verso la porta
        elapsedTime = 0;
        while (elapsedTime < halfTime)
        {
            elapsedTime += Time.unscaledDeltaTime;
            t = Mathf.Clamp01(elapsedTime / halfTime);
            virtualCamera.m_Lens.OrthographicSize = Mathf.Lerp(7f, finalSize, t);
            yield return null;  // Attende fino al frame successivo
        }

        // Seconda fase: pan indietro alla posizione iniziale
        elapsedTime = 0;
        while (elapsedTime < halfTime)
        {
            elapsedTime += Time.unscaledDeltaTime;
            t = Mathf.Clamp01(elapsedTime / halfTime);
            virtualCamera.m_Lens.OrthographicSize = Mathf.Lerp(finalSize, 7f, t);
            yield return null;  // Attende fino al frame successivo
        }
    }
}
