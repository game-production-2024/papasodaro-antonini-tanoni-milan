using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorGFX : MonoBehaviour
{
    public List<GameObject> conveyor = new List<GameObject>();
    public Transform parentObject;
    public GameObject prefab;
    public GameObject start;
    public GameObject end;
    public Vector2 startCoo;
    public Vector2 endCoo; 
    public int numberOfPrefabs;
    public Vector3 rotationSpeed = new Vector3(0, 0, 0); // Velocit� di rotazione per ciascun asse

    void Start()
    {
        startCoo = start.transform.position;
        endCoo = end.transform.position;
        SpawnPrefabs();
    }

    void SpawnPrefabs()
    {
        for (int i = 0; i <= numberOfPrefabs; i++)
        {
            float t = (float)i / numberOfPrefabs; // Calcolare la frazione dell'interpolazione
            Vector3 position = Vector3.Lerp(startCoo, endCoo, t); // Calcolare la posizione interpolata
            GameObject spawnedPrefab = Instantiate(prefab, position, start.transform.rotation, parentObject); // Instanziare il prefab alla posizione calcolata
            conveyor.Add(spawnedPrefab); // Aggiungere il prefab alla lista
        }
    }

    void Update()
    {
        foreach (GameObject prefab in conveyor) 
        {
            if (prefab.transform.position.x > endCoo.x)
            {
                prefab.transform.position = new Vector2(prefab.transform.position.x - (endCoo.x - startCoo.x), prefab.transform.position.y);
                return;
            }
            if (prefab.transform.position.x < startCoo.x)
            {
                prefab.transform.position = new Vector2(prefab.transform.position.x + (endCoo.x - startCoo.x), prefab.transform.position.y);
            }
        }
    }

    public void SetRotationSpeed(int value)
    {
        rotationSpeed = new Vector3 (0,0,value);
    }
}
