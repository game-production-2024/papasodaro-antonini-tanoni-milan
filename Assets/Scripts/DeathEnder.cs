using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathEnder : MonoBehaviour
{
    public FiniteStateMachine fsm;
    public PlayerParameters playerParameters;

    public void EndDeath()
    {
        playerParameters.physicalPlayer.transform.position = playerParameters.resetPosition;
        fsm.animator.Play("Idle_Rampino_Corretta");
        fsm.ChangeState<PlayerIdle>();

        if (playerParameters.isBossFight)
        {
            playerParameters.cat.ResetCat();
        }
    }
}
