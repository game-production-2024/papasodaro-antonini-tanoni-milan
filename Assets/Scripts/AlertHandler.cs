using UnityEngine;
using TMPro;  // Per TextMeshPro

public class AlertHandler : MonoBehaviour
{
    // Componenti pubblici per essere assegnati dall'Inspector
    public SpriteRenderer spriteRenderer;  // Per cambiare l'immagine
    public Sprite newSprite;  // La nuova immagine da visualizzare
    public TMP_Text notificationText;  // Il campo testo di TextMeshPro
    public string message;  // Il messaggio da visualizzare
    public Animator animator;  // Il componente Animator
    public bool stopSpawner;  // Boolean per bloccare lo spawner
    public FiniteStateMachine fsm;
    public GrapplingHookController grapplingHookController;

    bool isAllert;

    // Funzione che gestisce l'aggiornamento
    public void TriggerAlert(Sprite spriteToSet, string messageToSet, GameObject objectToDestroy)
    {
        Debug.Log("Triggerato");
        // Imposta la nuova sprite
        if (spriteRenderer != null && spriteToSet != null)
        {
            spriteRenderer.sprite = spriteToSet;
        }

        // Imposta il nuovo messaggio
        if (notificationText != null && !string.IsNullOrEmpty(messageToSet))
        {
            notificationText.text = messageToSet;
        }

        isAllert = true;

        animator.SetBool("isAllert", isAllert);

        

        // Distruggi l'oggetto che ha attivato il trigger (se esiste)
        if (objectToDestroy != null)
        {
            grapplingHookController.DestroyHook();
            grapplingHookController.DestroyJoint();
            fsm.ChangeState<PlayerIdle>();
            Destroy(objectToDestroy);
            
        }

      
    }

    void ResetAnimation()
    {
        isAllert = false;
        animator.SetBool("isAllert", isAllert);
    }
}
