using UnityEngine;

public class TeleportObject : MonoBehaviour
{
    // Assegna l'oggetto da teletrasportare dall'Inspector
    public GameObject objectToTeleport;

    // Assegna l'oggetto destinazione dall'Inspector
    public Transform targetTransform;

    // Funzione per teletrasportare l'oggetto
    public void Teleport()
    {
        if (objectToTeleport != null && targetTransform != null)
        {
            objectToTeleport.transform.position = targetTransform.position;
        }
        else
        {
            Debug.LogWarning("Uno degli oggetti non � stato assegnato nell'Inspector.");
        }
    }
}
