using Unity.VisualScripting;
using UnityEngine;

public class AlertTrigger : MonoBehaviour
{
    public AlertHandler alertHandler;  // Riferimento allo script AlertHandler
    public Sprite spriteToSet;  // La nuova sprite che sar� passata all'AlertHandler
    public string messageToSet;  // Il nuovo messaggio che sar� passato all'AlertHandler
    public GameObject dialogo;
    public bool stopSpawnerFlag;  // Flag per impostare stopSpawner


    // Metodo per gestire il trigger con i collider 2D
    public void OnTriggerEnter2D(Collider2D other)
    {


        // Quando il trigger 2D viene attivato, chiama la funzione TriggerAlert
        if (other.CompareTag("Anchor"))
        {
            // Chiama la funzione TriggerAlert, passando l'oggetto che ha attivato il trigger
            alertHandler.TriggerAlert(spriteToSet, messageToSet, this.gameObject);
            Debug.Log("Provo ad inviare notifica");

            if (stopSpawnerFlag == true)
            {
                dialogo.SetActive(true);
            }
        }
        else if (other.CompareTag("Player"))
            
        {
            alertHandler.TriggerAlert(spriteToSet, messageToSet, this.gameObject);
        }
       
        
    }
}
