using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class CameraTemporaryPan : MonoBehaviour
{
    private float elapsedTime = 0;
    private float halfTime;
    private float t = 0;
    public float duration;
    public Transform doorPosition;
    public Transform cameraFollowPosition;
    public UnityEvent beforePan;
    public UnityEvent onPan;
    public UnityEvent afterPan;

    public void CameraPan()
    {
        // Avvia la coroutine per il movimento della camera
        StartCoroutine(PanCamera());
    }

    private IEnumerator PanCamera()
    {
        // Salva la posizione iniziale
        Vector3 startPosition = cameraFollowPosition.position;
        Time.timeScale = 0f;  // Ferma il tempo di gioco

        // Calcola il tempo a met� della durata
        halfTime = duration / 2;

        beforePan.Invoke();

        // Prima fase: pan verso la porta
        elapsedTime = 0;
        while (elapsedTime < halfTime)
        {
            elapsedTime += Time.unscaledDeltaTime;
            t = Mathf.Clamp01(elapsedTime / halfTime);
            cameraFollowPosition.position = Vector2.Lerp(startPosition, doorPosition.position, t);
            yield return null;  // Attende fino al frame successivo
        }

        onPan.Invoke();

        // Seconda fase: pan indietro alla posizione iniziale
        elapsedTime = 0;
        while (elapsedTime < halfTime)
        {
            elapsedTime += Time.unscaledDeltaTime;
            t = Mathf.Clamp01(elapsedTime / halfTime);
            cameraFollowPosition.position = Vector2.Lerp(doorPosition.position, startPosition, t);
            yield return null;  // Attende fino al frame successivo
        }

        afterPan.Invoke();

        // Ripristina il tempo di gioco
        Time.timeScale = 1;
    }
}
