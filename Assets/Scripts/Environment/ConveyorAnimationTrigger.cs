using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorAnimationTrigger : MonoBehaviour
{
    public List<Animator> conveyorUnitList = new List<Animator>();

    void Awake()
    {
        conveyorUnitList.Clear();

        Animator[] conveyors = GetComponentsInChildren<Animator>();

        foreach (Animator conveyor in conveyors)
        {
            conveyorUnitList.Add(conveyor);
        }
    }

    public void TriggerConveyorAnimation()
    {
        foreach (Animator conveyor in conveyorUnitList)
        {
            conveyor.SetBool("IsActive", true);
        }
    }
}
