using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxDropper : MonoBehaviour
{
    public GameObject objPrefab;
    public GameObject obj;
    public GameObject origin;
    public Rigidbody2D rb;
    private BoxCollider2D bc;
    public float dropSpeed;


    void Start()
    {
        StartCoroutine(SpawnObjectAfterDelay(dropSpeed));
    }

    IEnumerator SpawnObjectAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);

        obj = Instantiate(objPrefab, origin.transform.position + new Vector3(0 ,-1f ,0), Quaternion.identity);

        StartCoroutine(SpawnObjectAfterDelay(delay));
    }
}
