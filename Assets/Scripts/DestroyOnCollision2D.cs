using UnityEngine;

public class DestroyOnCollision2D : MonoBehaviour
{
    // Metodo chiamato quando avviene una collisione fisica 2D
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Distrugge l'oggetto con cui � entrato in collisione
        Destroy(collision.gameObject);
    }
}
