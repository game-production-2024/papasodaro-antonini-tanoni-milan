using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowPointer : MonoBehaviour
{
    public PlayerParameters playerParameters;
    public GrapplingHookController grapplingHookController;
    public SpriteRenderer spriteRenderer;
    public Sprite spriteGreen;
    public Sprite spriteRed;
    private Vector2 direction;
    private Vector2[] directionVectors = new Vector2[]
    {
        new Vector2(1, 0),   // 0� - Est
        new Vector2(1, 1),   // 45� - Nord-Est
        new Vector2(0, 1),   // 90� - Nord
        new Vector2(-1, 1),  // 135� - Nord-Ovest
        new Vector2(-1, 0),  // 180� - Ovest
        new Vector2(-1, -1), // 225� - Sud-Ovest
        new Vector2(0, -1),  // 270� - Sud
        new Vector2(1, -1)   // 315� - Sud-Est
    };

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        switch (playerParameters.version)
        {
            case Version.Versione1:
                direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - playerParameters.pivot.position;
                direction = direction.normalized;
                break;
            case Version.Versione2:
                direction = new Vector2(playerParameters.horizontalMovement, 1);
                break;
            case Version.Versione3:
                direction = new Vector2(playerParameters.rb.velocity.x, Mathf.Max(Mathf.Abs(playerParameters.rb.velocity.x),1));
                direction = direction.normalized;
                break;
            case Version.Versione4:
                direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - playerParameters.pivot.position;
                direction = GetDirectionVector(direction);
                break;
        }

        gameObject.transform.position = playerParameters.pivot.position + (Vector3)direction * 2;
        gameObject.transform.up = direction;

        if ((playerParameters.line.enabled) || (grapplingHookController.redCross.activeSelf))
        {
            spriteRenderer.enabled = false; 
        }
        else
        {
            spriteRenderer.enabled = true;
        }

        RaycastHit2D hit = Physics2D.Raycast(playerParameters.pivot.transform.position, direction, grapplingHookController.maxHookDistance, grapplingHookController.hookInteractionLayer);

        if ((hit.collider != null) && (hit.collider.gameObject.GetComponent<Hittable>() is Hookable))
        {
            spriteRenderer.sprite = spriteGreen;
            return;
        }

        spriteRenderer.sprite = spriteRed;

    }

    Vector2 GetDirectionVector(Vector2 input)
    {
        if (input == Vector2.zero)
        {
            return Vector2.zero; // Se il vettore in ingresso � zero, restituisce zero
        }

        float angle = Mathf.Atan2(input.y, input.x) * Mathf.Rad2Deg;
        if (angle < 0)
        {
            angle += 360;
        }

        int index = Mathf.RoundToInt(angle / 45) % 8;
        return directionVectors[index];
    }
}