using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class BoxFaller : MonoBehaviour
{
    [SerializeField] private string otherTag;
    private Rigidbody2D rb;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(otherTag))
        {
            rb = other.AddComponent<Rigidbody2D>();
            rb.mass = 1000f;
            rb.velocity = new Vector2(6f, 0);
        }
    }
}
