using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    public Transform pakoPosition;
    public float catSpeed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if (catSpeed < 0)
        {
            gameObject.transform.localScale = new Vector3 (-13f,11f,1f);
        }
        else
        {
            gameObject.transform.localScale = new Vector3(13f, 11f, 1f);
        }
        rb.velocity = new Vector3(catSpeed, 0f, 0f);
    }

    public void SetSpeed(int value)
    {
        catSpeed = value;
    }

    public void ResetCat()
    {
       transform.position = new Vector2 (pakoPosition.position.x - 20f, transform.position.y);
    }
}
