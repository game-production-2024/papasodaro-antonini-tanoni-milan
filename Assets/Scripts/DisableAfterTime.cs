using UnityEngine;

public class DisableAfterTime : MonoBehaviour
{
    // Metodo Start viene chiamato una volta all'inizio
    private void Start()
    {
        // Avvia un timer di 1.5 secondi che poi disattiva l'oggetto
        Invoke("DisableObject", 1.5f);
    }

    // Funzione che disattiva l'oggetto
    private void DisableObject()
    {
        gameObject.SetActive(false);
    }
}
