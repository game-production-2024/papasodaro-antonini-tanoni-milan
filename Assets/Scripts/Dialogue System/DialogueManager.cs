using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager Instance;

    public RawImage Talking_Pako;
    public RawImage Talking_Npc;
    public GameObject namePako;
    public GameObject nameNpc;
    public TextMeshProUGUI characterName;
    public TextMeshProUGUI dialogueArea;
    public GameObject choicesContainer;
    public Button choiceButtonPrefab;
    public Button continueButton;
    private Queue<DialogueLine> lines;
    private DialogueLine currentLine;

    public bool isDialogueActive = false;

    public float typingSpeed = 0.2f;

    public Animator animator;

    public GameObject interactionBox;

    public KeyCode interactionKey = KeyCode.E;

    public KeyCode skipKey = KeyCode.Space;

    public FiniteStateMachine fsm;

    private Dialogue currentDialogue;

   
    private bool isTyping = false;
    private Coroutine typingCoroutine;

    // Variabili per lo skip delay
    public float skipDelay = 0.1f; // Delay tra una pressione e l'altra
    private float lastSkipTime = 0f; // Tempo dell'ultima pressione

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        lines = new Queue<DialogueLine>();
    }

    private void Update()
    {
        if (interactionBox.activeSelf && Input.GetKeyDown(interactionKey))
        {
            if (currentDialogue != null && !isDialogueActive)
            {
                interactionBox.SetActive(false);
                StartDialogue(currentDialogue);
            }
        }

        // Skip con delay e controllo delle scelte
        if (isDialogueActive && Input.GetKeyDown(skipKey) && Time.time - lastSkipTime > skipDelay)
        {
            // Verifica se ci sono scelte, in tal caso non permettiamo lo skip
            if (currentLine != null && (currentLine.choices == null || currentLine.choices.Count == 0))
            {
                lastSkipTime = Time.time; // Aggiorna l'ultimo tempo di skip
                OnContinueButtonClicked();
            }
        }
    }

    public void StartDialogue(Dialogue dialogue)
    {
        isDialogueActive = true;
        animator.Play("show");
        lines.Clear();
        fsm.ChangeState<PlayerTalking>();
        foreach (DialogueLine dialogueLine in dialogue.dialogueLines)
        {
            lines.Enqueue(dialogueLine);
        }
        DisplayNextDialogueLine();
    }

    public void DisplayNextDialogueLine()
    {
        if (lines.Count == 0)
        {
            EndDialogue();
            return;
        }

        currentLine = lines.Dequeue();
        if (currentLine.character.name == "Pako")
        {
            Talking_Pako.gameObject.SetActive(true);
            Talking_Npc.gameObject.SetActive(false);
            namePako.SetActive(true);
            nameNpc.SetActive(false);
            //SoundSystem.Instance.PlaySoundEffect("Pako");
        }
        else
        {
            Talking_Pako.gameObject.SetActive(false);
            Talking_Npc.gameObject.SetActive(true);
            namePako.SetActive(false);
            nameNpc.SetActive(true);
            Talking_Npc.texture = currentLine.character.icon.texture;
            //SoundSystem.Instance.PlaySoundEffect(currentLine.character.name);
        }

        characterName.text = currentLine.character.name;
        StopAllCoroutines();
        typingCoroutine = StartCoroutine(TypeSentence(currentLine));
    }

    IEnumerator TypeSentence(DialogueLine dialogueLine)
    {
        isTyping = true;
        dialogueArea.text = "";
        foreach (char letter in dialogueLine.line.ToCharArray())
        {
            dialogueArea.text += letter;
            MakeTalkingSound();
            yield return new WaitForSeconds(typingSpeed);
        }
        isTyping = false;
        if (dialogueLine.choices != null && dialogueLine.choices.Count > 0)
        {
            ShowChoices(dialogueLine.choices);
        }
        else
        {
            continueButton.gameObject.SetActive(true);
        }
    }

    void ShowChoices(List<DialogueChoice> choices)
    {
        foreach (Transform child in choicesContainer.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (var choice in choices)
        {
            var button = Instantiate(choiceButtonPrefab, choicesContainer.transform);
            var buttonText = button.GetComponentInChildren<TextMeshProUGUI>();
            if (buttonText == null)
            {
                buttonText = button.GetComponentInChildren<Transform>().GetComponentInChildren<TextMeshProUGUI>();
            }
            buttonText.text = choice.choiceText;
            button.onClick.AddListener(() => OnChoiceSelected(choice.nextDialogue));
        }

        choicesContainer.SetActive(true);
        continueButton.gameObject.SetActive(false);
    }

    public void OnContinueButtonClicked()
    {
        if (isTyping)
        {
            StopCoroutine(typingCoroutine);
            dialogueArea.text = currentLine.line;
            isTyping = false;
            continueButton.gameObject.SetActive(true);
        }
        else
        {
            continueButton.gameObject.SetActive(false);
            DisplayNextDialogueLine();
        }
    }

    void OnChoiceSelected(Dialogue nextDialogue)
    {
        choicesContainer.SetActive(false);
        continueButton.gameObject.SetActive(true);
        StartDialogue(nextDialogue);
    }

    public void EndDialogue()
    {
        isDialogueActive = false;
        animator.Play("hide");
        currentDialogue.OnDialogueEnd();
    }

    public void SetCurrentDialogue(Dialogue dialogue)
    {
        currentDialogue = dialogue;
    }

    public void MakeTalkingSound()
    {
        if(currentLine.character.name == "Pako")
        {
            SoundSystem.Instance.PlaySoundEffect("PakoTalking");
            return;
        }
        if (currentLine.character.name == "Rub-bik")
        {
            SoundSystem.Instance.PlaySoundEffect("Rub-bikTalking");
            return;
        }
        if (currentLine.character.name == "Rac-catto")
        {
            SoundSystem.Instance.PlaySoundEffect("Rac-cattoTalking");
            return;
        }

    }
}
