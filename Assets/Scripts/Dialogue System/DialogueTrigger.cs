using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.Events;




[System.Serializable]
public class DialogueCharacter
{
    public string name;
    public Sprite icon;
}

[System.Serializable]
public class DialogueChoice
{
    public string choiceText;
    public Dialogue nextDialogue;
}

[System.Serializable]
public class DialogueLine
{
    public DialogueCharacter character;
    [TextArea(3, 10)]
    public string line;
    public List<DialogueChoice> choices; 
}

[System.Serializable]
public class Dialogue
{
    public List<DialogueLine> dialogueLines = new List<DialogueLine>();
    public UnityEvent onDialogueEnd;

    public void OnDialogueEnd()
    {
        onDialogueEnd.Invoke();
    }
}




public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public bool isMandatory;  

    public void TriggerDialogue()
    {
        DialogueManager.Instance.SetCurrentDialogue(dialogue);
        DialogueManager.Instance.StartDialogue(dialogue);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !DialogueManager.Instance.isDialogueActive)
        {
            if (isMandatory)
            {
                isMandatory = false;
                TriggerDialogue();
            }
            else
            {
                DialogueManager.Instance.SetCurrentDialogue(dialogue);
                DialogueManager.Instance.interactionBox.SetActive(true);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        DialogueManager.Instance.interactionBox.SetActive(false);
    }
}

