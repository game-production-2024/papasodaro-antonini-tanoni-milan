using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

public class GrapplingHookController : MonoBehaviour
{
    [Header("Tunables")]
    public float maxHookDistance;

    [Header("Resources")]
    public GameObject anchorPrefab;
    public GameObject redCross;
    public LayerMask hookInteractionLayer;
    public GameObject hookArm1;
    public GameObject hookArm2;
    
    [Header("Script links")]
    public PlayerParameters playerParameters;
    public FiniteStateMachine fsm;
    
    [HideInInspector] public float hookedRopeLength;
    [HideInInspector] public Vector2 direction;
    [HideInInspector] public Vector2 instantiatePosition;

    [HideInInspector] public GameObject anchor;
    [HideInInspector] public SpringJoint2D joint;
    [HideInInspector] public Hittable hookHit;

    private Vector2[] directionVectors = new Vector2[]
    {
        new Vector2(1, 0),   // 0� - Est
        new Vector2(1, 1),   // 45� - Nord-Est
        new Vector2(0, 1),   // 90� - Nord
        new Vector2(-1, 1),  // 135� - Nord-Ovest
        new Vector2(-1, 0),  // 180� - Ovest
        new Vector2(-1, -1), // 225� - Sud-Ovest
        new Vector2(0, -1),  // 270� - Sud
        new Vector2(1, -1)   // 315� - Sud-Est
    };

    public void InitializeHook()
    {
        if (anchor == null)
        {
            anchor = Instantiate(anchorPrefab, instantiatePosition, Quaternion.identity);
        }
        if (!playerParameters.line.enabled)
        {
            playerParameters.line.positionCount = 2;
            playerParameters.line.SetPosition(0, playerParameters.pivot.position);
            playerParameters.line.SetPosition(1, instantiatePosition);
            playerParameters.line.enabled = true;
        }

        hookArm1.SetActive(true);   
        hookArm2.SetActive(true);
        playerParameters.SetLeftHook(false);
        playerParameters.SetRightHook(false);
    }

    public void TryShootingHook()
    {
        switch (playerParameters.version)
        {
            case Version.Versione1:
                direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - playerParameters.pivot.position;
                direction = direction.normalized;
                break;
            case Version.Versione2:
                direction = new Vector2(playerParameters.horizontalMovement, 1);
                break;
            case Version.Versione3:
                direction = new Vector2(playerParameters.rb.velocity.x, Mathf.Abs(playerParameters.rb.velocity.x));
                direction = direction.normalized;
                break;
            case Version.Versione4:
                direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - playerParameters.pivot.position;
                direction = GetDirectionVector(direction);
                break;
        }

        RaycastHit2D hit = Physics2D.Raycast(playerParameters.pivot.transform.position, direction, maxHookDistance, hookInteractionLayer);



        if (hit.collider == null)
        {
            //Animazione rampino che raggiunge la massima estensione non si aggancia e si ritrae
            SoundSystem.Instance.PlaySoundEffect("Fail");
            redCross.SetActive(true);
            return;
        }

        hookHit = hit.collider.gameObject.GetComponent<Hittable>();
        instantiatePosition = hit.point;
        hookedRopeLength = hit.distance;

        if (hookHit is Hookable)
        {
            SoundSystem.Instance.PlaySoundEffect("Shoot");
            playerParameters.animator.SetBool("IsSwinging", true);
            if (playerParameters.IsGrounded())
            {
                fsm.ChangeState<PlayerHookGround>();
            }
            else
            {
                fsm.ChangeState<PlayerHookAir>();
            }
            return;
        }
        
        if (hookHit is Unhookable)
        {
            redCross.SetActive(true);
            //Animazione rampino che colpisce non si aggancia e si ritrae
        }
    }

    public int SignedDist()
    {
        float currentDistance = Vector3.Distance(playerParameters.pivot.position, anchor.transform.position);

        if (currentDistance >= hookedRopeLength)
        {
            if (playerParameters.pivot.position.x < anchor.transform.position.x)
            {
                playerParameters.FacingRight();
                playerParameters.pivot.localScale = new Vector3(1f, 1f, 1f);
                return 1;
            }
            else
            {
                playerParameters.FacingLeft();
                playerParameters.pivot.localScale = new Vector3(-1f, 1f, 1f);
                return -1;
            }
        }
        else
        {
            if (playerParameters.pivot.position.x < anchor.transform.position.x)
            {
                playerParameters.FacingRight();
                playerParameters.pivot.localScale = new Vector3(1f, 1f, 1f);
            }
            else
            {
                playerParameters.FacingLeft();
                playerParameters.pivot.localScale = new Vector3(-1f, 1f, 1f);
            }
            return 0;
        }
    }

    public void SetJoint()
    {
        joint = playerParameters.physicalPlayer.AddComponent<SpringJoint2D>();
        joint.enableCollision = true;
        joint.dampingRatio = 0.9f;
        joint.frequency = 1.0f;
        joint.connectedBody = anchor.GetComponent<Rigidbody2D>();
    }

    public float ApplySwingForce()
    {
        float currentDistance = Mathf.Abs(playerParameters.pivot.transform.position.x - anchor.transform.position.x);
        float distanceX = hookedRopeLength * Mathf.Sin(0.6f);

        if (currentDistance > distanceX)
        {
            Debug.LogWarning("Distance is greater than the maximum distance. No force applied.");
            return 0;
        }

        float forceMagnitude = (distanceX == 0) ? playerParameters.maxForce : Mathf.Lerp(playerParameters.maxForce, 0, currentDistance / distanceX);
        return forceMagnitude;
    }

    public void PullOnRope()
    {
        if (joint.distance > 1)
        {
            if (joint != null)
            {
                Vector2 speedIncreaser = (anchor.transform.position - playerParameters.pivot.position).normalized * playerParameters.pullSpeed;
                playerParameters.rb.velocity += speedIncreaser;
                hookedRopeLength = hookedRopeLength - (joint.distance - (anchor.transform.position - playerParameters.pivot.position).magnitude);
                joint.distance = (anchor.transform.position - playerParameters.pivot.position).magnitude;
                playerParameters.line.SetPosition(0, playerParameters.pivot.position);
            }
        }

    }

    public void RepelOnRope()
    {
        if (joint.distance < playerParameters.maxRopeLenght)
        {
            if (joint != null)
            {
                hookedRopeLength = hookedRopeLength + playerParameters.repelSpeed;
                joint.distance = joint.distance + playerParameters.repelSpeed;
                playerParameters.line.SetPosition(0, playerParameters.pivot.position);
            }
        }
    }

    Vector2 GetDirectionVector(Vector2 input)
    {
        if (input == Vector2.zero)
        {
            return Vector2.zero; // Se il vettore in ingresso � zero, restituisce zero
        }

        float angle = Mathf.Atan2(input.y, input.x) * Mathf.Rad2Deg;
        if (angle < 0)
        {
            angle += 360;
        }

        int index = Mathf.RoundToInt(angle / 45) % 8;
        return directionVectors[index];
    }

    public void UpdateHook()
    {
        hookHit.UpdateActiveAnchor(ref anchor, ref playerParameters.pivot, playerParameters.line, ref hookedRopeLength);
        UpdateArm();
    }

    public void UpdateJoint()
    {
        UpdateHook();
        if (anchor.GetComponent<Rigidbody2D>() != joint.connectedBody)
        {
            joint.connectedBody = anchor.GetComponent<Rigidbody2D>();
        }
    }

    public void UpdateArm()
    {
        playerParameters.SetLeftHook(false);
        playerParameters.SetRightHook(false);
        if (playerParameters.hookArmSwingSx.enabled)
        {
            SetArmDirection(hookArm1);
        }
        else
        {
            SetArmDirection(hookArm2);
        }
    }

    public void SetArmDirection(GameObject arm)
    {
        Vector2 direction = anchor.transform.position - arm.transform.position;
        direction = direction.normalized;
        arm.transform.right = direction;
        playerParameters.line.SetPosition(0, arm.transform.position);
    }

    public void DestroyHook()
    {
        if (hookHit != null)
        {
            hookHit.DestroyHittable();
        }
        if (anchor != null)
        {
            Destroy(anchor);
        }
        playerParameters.line.enabled = false;
        playerParameters.animator.SetBool("IsSwinging", false);

        hookArm1.SetActive(false);
        hookArm2.SetActive(false);
        if (playerParameters.pivot.localScale.x == 1)
        {
            playerParameters.FacingRight();
        }
        else
        {
            playerParameters.FacingLeft();
        }
    }

    public void DestroyJoint()
    {
        if (joint != null)
        {
            Destroy(joint);
        }
    }
}
