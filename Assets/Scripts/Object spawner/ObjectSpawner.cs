using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    // Prefab degli oggetti da spawnare
    public GameObject prefab1;
    public GameObject prefab2;
    public AlertHandler alertHandler;
    public AlertTrigger trigger;
    public GameObject dialogo;
    
    
    // Rapporto di spawn (quante volte prefab1 viene spawnato rispetto a prefab2)
    [Range(0, 1)] // Slider per regolare il rapporto dall'inspector
    public float spawnRatio = 0.5f; // 0.5 = 50% di prefab1 e 50% di prefab2

    // Velocit� di movimento sull'asse X
    public float objectSpeed = 0f;

    // Intervallo tra uno spawn e l'altro
    public float spawnInterval = 2f;

    private void Start()
    {
        // Avvia il ciclo di spawn
        InvokeRepeating("SpawnObject", 0f, spawnInterval);
    }

    private void SpawnObject()
    {
       
        // Genera un numero casuale per determinare quale prefab spawnare
        GameObject prefabToSpawn = Random.value < spawnRatio ? prefab1 : prefab2;

        // Spawn del prefab
        GameObject spawnedObject = Instantiate(prefabToSpawn, transform.position, Quaternion.identity);

        
        trigger = spawnedObject.GetComponent<AlertTrigger>();
        trigger.alertHandler = alertHandler;
        trigger.dialogo = dialogo;


        // Imposta la velocit� sull'asse X
        Rigidbody2D rb = spawnedObject.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            rb.velocity = new Vector2(objectSpeed, rb.velocity.y);
        }
        else
        {
            Debug.LogWarning("L'oggetto spawnato non ha un Rigidbody2D!");
        }
    }
}
