using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDetectorCheckpoint : MonoBehaviour
{
    [SerializeField] private string otherTag;
    [SerializeField] private PlayerParameters playerParameters;
    [SerializeField] private Transform checkpoint;
    [SerializeField] private GameObject checkpointParticles;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(otherTag))
        {
            playerParameters.SetCheckpoint(checkpoint);   
            ActivateCheckpointParticles();
        }
    }

    public void ActivateCheckpointParticles()
    {
        checkpointParticles.SetActive(true);
    }
}
