using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnfreezeX : MonoBehaviour
{
    public Rigidbody2D rb;

    public void Unfreeze()
    {
        rb.constraints &= ~RigidbodyConstraints2D.FreezePositionX;
    }
}
