using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSprite : MonoBehaviour
{
    public Sprite newSprite;
    public SpriteRenderer spriteRenderer;

    public void DoChangeSprite()
    {
        spriteRenderer.sprite = newSprite;
    }
}
