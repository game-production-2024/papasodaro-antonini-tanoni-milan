using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hud : MonoBehaviour
{

    public GameObject Coord;
    public GameObject HudLMB;
    public GameObject HudRMB;
    public GameObject HudS;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Coord.transform.position.x> 49.07)
        {
            HudLMB.SetActive(true);
        }
        if (Coord.transform.position.x > 170)
        {
            HudRMB.SetActive(true);
        }
        if (Coord.transform.position.x > 250)
        {
            HudS.SetActive(true);
        }

    }
}
