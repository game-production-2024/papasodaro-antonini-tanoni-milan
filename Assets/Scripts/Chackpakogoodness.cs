using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpakogoodness : MonoBehaviour
{
    public GameObject dialogoV1;
    public GameObject dialogoV2;
    public GameObject dialogoV3;
    public PlayerParameters playerParameters;

    public void SetLastDialogue()
    {
        if (playerParameters.goodnessOfPako <= 3)
        {
            dialogoV1.SetActive(true);
        }
        else if (playerParameters.goodnessOfPako < 5)
        {
            dialogoV2.SetActive(true);
        }
        else
        {
            dialogoV3.SetActive(true);
        }
    }
}