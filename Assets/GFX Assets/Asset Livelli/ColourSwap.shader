Shader "Custom/ExactColorSwap"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {} // the sprite we want to apply the colour swap to 
        _OriginalColor("Original Color", Color) = (1,1,1,1) // og colour of the sprite 
        _TargetColor("Target Color", Color) = (1,1,1,1) // what i want the og colour to become 
        _Tolerance("Tolerance", Range(0, 0.01)) = 0.001  
    }

    SubShader
    {
        Tags { "RenderType" = "Transparent" } // treat the object as transparent 
        Blend SrcAlpha OneMinusSrcAlpha // the blending mode can handle transparency 
        ZWrite Off

        Cull Off
        Pass // define what we do for each pixel 
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _OriginalColor;
            float4 _TargetColor;
            float _Tolerance;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            half4 frag(v2f i) : SV_Target // function applied to every pixel 
            {
                half4 col = tex2D(_MainTex, i.uv);

                if (col.a == 0) // the alpha of the pixel must be equal to zero 
                {
                    return half4(0, 0, 0, 0);
                }

                if (length(col - _OriginalColor) < _Tolerance) // if the current pixel is close enough to our og colour to be considered a colour swap 
                {
                    return half4(_TargetColor.rgb, col.a); // it gives the rgb and alpha of the target colour 
                }

                return col;
            }

            ENDCG
        }


    }
}